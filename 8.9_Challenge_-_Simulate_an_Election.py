from random import randint

num_Cand_A_wins_election = 0
num_votes = 10_000

for vote in range(num_votes):
    
    num_Cand_A_wins_region = 0
    
    if randint(1, 100) <= 87:
        num_Cand_A_wins_region = num_Cand_A_wins_region + 1
    if randint(1, 100) <= 65:
        num_Cand_A_wins_region = num_Cand_A_wins_region + 1
    if randint(1, 100) <= 17:
        num_Cand_A_wins_region = num_Cand_A_wins_region + 1
        
    if num_Cand_A_wins_region >= 2:
        num_Cand_A_wins_election = num_Cand_A_wins_election + 1
        
print(f"Chance of wining by Candidate A = {((num_Cand_A_wins_election / num_votes) * 100):.1f}%")
