import random

def num_flips_in_trial():
    """Simulate repeatedly a coing until both heads and tails are seen and returns count of flips"""
    flip_result = random.randint(0, 1)
    flip_count = 1
    while flip_result == random.randint(0, 1):
        flip_count = flip_count + 1
    flip_count = flip_count + 1
    return flip_count 

sum_of_flips = 0
num_trials = 10_000

for trial in range(num_trials):
    sum_of_flips = sum_of_flips + num_flips_in_trial()

average_flips_per_trial = sum_of_flips / num_trials

print(f"The average number of coin flips is {average_flips_per_trial} per trial")
