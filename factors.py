positive_integer = int(input("Enter a positive integer: "))

if positive_integer >= 1:
    for i in range(1, positive_integer + 1):
        modulo = positive_integer % i
        if modulo == 0:
            print(f"{i} is factor of {positive_integer}")
else:
    print("This isn't a positive integer.")

              
