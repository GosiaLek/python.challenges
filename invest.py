def invest(amount, rate, years):
    """Tracks the growing amount of an investement over time."""
    for year in range(1, years + 1):
        amount = amount + (amount * rate)
        print(f"year {year}: ${amount:.2f}")

amount = float(input("Enter the initial deposit for an investment: "))
rate = float(input("Enter the percentage to increases the amount (as decimal number) : "))
years = int(input("Enter a number of years: "))
invest(amount, rate, years)

