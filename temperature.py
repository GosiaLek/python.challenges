def convert_cel_to_far(C):
    """Return the Celsius temperature C converted to Fahrenheit."""
    F = C * (9 / 5) + 32
    return F

def convert_far_to_cel(F):
    """Return the Fahrenheit temperature F converted to Celsius."""
    C = (F - 32) * (5 / 9)
    return C

C = float(input("Enter a temperature in degrees C: "))
F = convert_cel_to_far(C)
print(f"{C} degrees C = {F:.2f} degrees F")

F = float(input("Enter a temperature in degrees F: "))
C = convert_far_to_cel(F)
print(f"{F} degrees F = {C:.2f} degrees C")
